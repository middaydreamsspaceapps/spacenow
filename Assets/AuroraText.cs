﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuroraText : MonoBehaviour {

	public float delay, waitBetween;
	public GameObject bluePanel;
   	private string updateText;
	private bool firstTime = true;
	private string [] AuroraTexts = {
		"An aurora, also known as polar lights, northern lights or southern lights is a type of natural light seen mostly in the high altitude regions.",
		"The dancing lights of the aurorae provide spectacular views on the ground, but also capture the imagination of scientists who study incoming energy and particles from the sun.",
		"Aurorae are one effect of such energetic particles, which can speed out from the sun.",
		"Aurorae are produced when the magnetosphere is sufficiently disturbed by the solar wind.",
		"A region that currently displays an aurora is called the 'Auroral Oval'.",
		"In northern latitudes, more specificly in Arctic region, the effect is known as the aurora borealis or the northern lights.",
		"In Southern latitudes, around Antarctic, the effect is known as the aurora australis or the southern lights.",
	};

    void Start()
    {
        StartCoroutine(ShowSunText());
    }

    IEnumerator ShowSunText()
    {
	for (int j = 0; j < AuroraTexts.Length; j++)
        {
			if(!firstTime){
				yield return new WaitForSeconds (waitBetween);
			}
			firstTime = false;
		for (int i = 0; i < AuroraTexts[j].Length; i++)
	            {
			updateText = AuroraTexts[j].Substring(0, i + 1);
	                GetComponent<Text>().text = updateText;
	                yield return new WaitForSeconds(delay);
	            }
        }
        yield return new WaitForSeconds(waitBetween);
        gameObject.SetActive(false);
		bluePanel.SetActive (false);
        yield return null;
    }
}
