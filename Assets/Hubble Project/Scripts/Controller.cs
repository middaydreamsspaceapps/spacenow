﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {


    public GameObject Hubble;
	private GameObject EarthCenter;
	public float HubbleRotationSpeed;
	private GameObject Earth;
	public float EarthRotationSpeed;
  

	// Use this for initialization
	void Start () {
		EarthCenter = GameObject.Find ("EarthCenter");
		Earth = GameObject.Find ("earth");
	}
	
	// Update is called once per frame
	void Update () {
	
		RotateEarth ();
        RotateHubble();
	}

	private void RotateEarth()
	{
		Earth.transform.Rotate (0, EarthRotationSpeed * Time.deltaTime, 0);
	}

  
    private void RotateHubble()
    {
       
        EarthCenter.transform.Rotate(0, 0, 0.02f);
        Hubble.transform.RotateAround(EarthCenter.transform.position, -EarthCenter.transform.right, 
            HubbleRotationSpeed * Time.deltaTime);        
    }

}
