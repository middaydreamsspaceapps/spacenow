﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class HubbleSceneManager : MonoBehaviour {

    //private static HubbleSceneManager instance;
    //public static HubbleSceneManager Instance
    //{
    //    get
    //    {
    //        return instance;
    //    }
    //}
    //void Awake()
    //{
    //    if (instance == null)
    //    {
    //        instance = this;
    //    }
    //    else if (instance != this)
    //    {
    //        Destroy(gameObject);
    //        return;
    //    }
    //    DontDestroyOnLoad(gameObject);
    //}

    public void LoadScene(int index)
    {
        SceneManager.LoadSceneAsync(index);
    }
    public void Quit()
    {
        Application.Quit();
    }
}
