﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewManager : MonoBehaviour {

    GameObject[] Buttons;
    public GameObject DescriptionPanel;
    public GameObject MainCam;
    public Transform HubbleRotation;
    public RotateObject rotateObject;
    

	// Use this for initialization
	void Start () {
       
        Buttons = GameObject.FindGameObjectsWithTag("Button");
        for(int i = 0; i < Buttons.Length; i++)
        {
            Buttons[i].SetActive(false);
        }
		
	}

    public void FullView()
    {
        rotateObject.rotSpeed = 380f;
        rotateObject.enabled = true;
        MainCam.SetActive(true);
        DescriptionPanel.SetActive(true);
        for (int i=0;i<Buttons.Length;i++)
        {
            Buttons[i].SetActive(false);
        }

    }
    public void PartsView()
    {
        rotateObject.rotSpeed = 0f;
        rotateObject.enabled = false;
        HubbleRotation.rotation = Quaternion.identity;
        MainCam.SetActive(true);
        DescriptionPanel.SetActive(false);
        for (int i = 0; i < Buttons.Length; i++)
        {
            Buttons[i].SetActive(true);
        }

    }
}
