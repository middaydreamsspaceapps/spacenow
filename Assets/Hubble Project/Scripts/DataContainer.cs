﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace mdd.hubble
{

    public class DataContainer : MonoBehaviour
    {


        public Data[] Datas;
        public Data data;
        //public Text DescriptionText;
        public GameObject mainCam;
        public Animator animator;
        GameObject TempCam;
        public static DataContainer Instance { get; private set; }


        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }

        }

        public void GetDescriptionData(string name)
        {
            animator.SetBool("IsIdle", true);
            animator.SetBool("IsPrimary", false);
            animator.SetBool("IsSecondary", false);
            mainCam.SetActive(false);
            if (TempCam != null)
            {
                TempCam.SetActive(false);
            }

            foreach (Data tempData in Datas)
            {
                if (tempData.Name.Equals(name))
                {
                    data = tempData;
                    //DescriptionText.text = tempData.Description;
                    tempData.Vcam.SetActive(true);
                    TempCam = tempData.Vcam;
                    //Debug.Log(tempData.Name);
                }
            }

            if (data.Name == "PrimaryMirror")
            {
                Debug.Log(data.Name);

                animator.SetBool("IsPrimary", true);
                animator.SetBool("IsSecondary", false);
                animator.SetBool("IsIdle", false);
            }
            else if (data.Name == "SecondaryMirror")
            {
                Debug.Log(data.Name);
                animator.SetBool("IsSecondary", true);
                animator.SetBool("IsPrimary", false);
                animator.SetBool("IsIdle", false);
            }
        }
    }
}





