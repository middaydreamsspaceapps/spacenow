﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBlackhole : MonoBehaviour
{
    public GameObject blackhole, anotherBlackhole, theWave, firstCircle, secondCircle, blackholeParticle, 
            secondBlackholeParticle, outerParticle;
    public float speed = 200f;

    void Start()
    {
        StartCoroutine(StarBlackhole());
    }

    void Update()
    {
        
    }

    IEnumerator StarBlackhole()
    {
        float spriteAlpha = 0.55f, circleSize = 40f, blackholeSize = 14f;
        bool startSize = false;
        while (true)
        {
            float xPosition = blackhole.transform.position.x;
            float anotherX = anotherBlackhole.transform.position.x;
            speed += 1.8f;
            xPosition += (xPosition < 0) ? 0.8f : 0;
            blackhole.transform.position = new Vector3(xPosition, blackhole.transform.position.y, blackhole.transform.position.z);
            anotherX -= (anotherX > 0) ? 0.8f : 0;
            anotherBlackhole.transform.position = new Vector3(anotherX, anotherBlackhole.transform.position.y,
            anotherBlackhole.transform.position.z);

            if (anotherX < 0)
            {
                anotherX = (-1) * anotherX;
            }

            if (anotherX <= 4.5f)
            {
                startSize = true;
                Color spriteColor = theWave.GetComponent<SpriteRenderer>().color;
                spriteAlpha -= 0.004f;
                spriteColor.a = spriteAlpha;
                theWave.GetComponent<SpriteRenderer>().color = spriteColor;
            }
            if (startSize && circleSize <=110f)
            {
                firstCircle.transform.localScale += new Vector3(0.09f, 0.09f, 0.09f);
                secondCircle.transform.localScale += new Vector3(0.09f, 0.09f, 0.09f);
                circleSize += 0.09f;
            }
            if(startSize && blackholeSize <= 38f)
            {
                blackholeParticle.transform.localScale += new Vector3(0.032f, 0.032f, 0.032f);
                secondBlackholeParticle.transform.localScale += new Vector3(0.032f, 0.032f, 0.032f);
                blackholeSize += 0.032f;
            }
            if (circleSize < 110f)
            {
                blackhole.transform.RotateAround(transform.position, -transform.up, speed * Time.deltaTime);
                anotherBlackhole.transform.RotateAround(transform.position, -transform.up, speed * Time.deltaTime);                
            }
            else
            {
                outerParticle.SetActive(true);
            }
            theWave.transform.RotateAround(transform.position, -transform.up, speed * Time.deltaTime);
            yield return new WaitForSeconds(0.01f);
        }
        yield return null;
    }
}
