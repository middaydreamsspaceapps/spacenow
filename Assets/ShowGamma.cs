﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;


public class GammaBurst
{
    public string name { get; set; }
    public string right_ascension { get; set; }
    public string declination { get; set; }
    public string detected_by { get; set; }
    public string date { get; set; }
    public string distance { get; set; }
    public double x_co { get; set; }
    public double y_co { get; set; }
    public double z_co { get; set; }
}

public class ShowGamma : MonoBehaviour
{
    /*
    Name 0, Right Ascension 1, Declination 2, Detected by 3, Date 4,
        Distance 5, Degree-X 6, Degree-Y 7, X 8, Y 9, Z 10*/

    public GameObject gamma_prefab, textPanel;
    public Text nameText, rightAscensionText, declinationText, detectedByText, detectedOnText, distanceText;
    public ArrayList gammaSources = new ArrayList();
    public Vector2 currentRotation;
    public float maxYAngle = 85f;
    private Vector3 cameraPosition, targetPosition;

    void Start()
    {
        cameraPosition = Camera.main.transform.position;
        targetPosition = Camera.main.transform.position;
        TextAsset textAsset = new TextAsset();
        textAsset = Resources.Load<TextAsset>("gamma_data");
        string[] all_gamma = textAsset.text.Split('\n');

        int ik = 0;

        string gammaNames = "";

        while (ik < all_gamma.Length)
        {
            var line = all_gamma[ik];
            var gamma_details = line.Split(',');
            GammaBurst aGamma = new GammaBurst();
            aGamma.name = gamma_details[0];
            aGamma.right_ascension = gamma_details[1];
            aGamma.declination = gamma_details[2];
            aGamma.detected_by = gamma_details[3];
            aGamma.date = gamma_details[4];
            aGamma.distance = gamma_details[5];
            aGamma.x_co = double.Parse(gamma_details[8]);
            aGamma.y_co = double.Parse(gamma_details[9]);
            aGamma.z_co = double.Parse(gamma_details[10]);       
            gammaSources.Add(aGamma);
            ik++;
        }
        StartCoroutine(ShowGammaBurst());
    }

    void Update()
    {
        Quaternion lookOnLook = Quaternion.LookRotation(targetPosition - cameraPosition);
        Camera.main.transform.rotation = Quaternion.Slerp(Camera.main.transform.rotation, lookOnLook, Time.deltaTime);
    }

    IEnumerator ShowGammaBurst()
    {
        int gamma_counter = 0;
        foreach(GammaBurst a_gamma in gammaSources)
        {
            targetPosition = new Vector3((float)a_gamma.x_co * 24f, (float)a_gamma.y_co * 24f, (float)a_gamma.z_co) * 24f;
            yield return new WaitForSeconds(6f);
            GameObject the_gamma = Instantiate(gamma_prefab, new Vector3((float)a_gamma.x_co * 24f,
                (float)a_gamma.y_co * 24f, (float)a_gamma.z_co) * 24f, Quaternion.identity);
            yield return new WaitForSeconds(1f);
            nameText.text = a_gamma.name;
            rightAscensionText.text = a_gamma.right_ascension;
            declinationText.text = a_gamma.declination;
            detectedByText.text = a_gamma.detected_by;
            detectedOnText.text = a_gamma.date.Split(';')[0]+","+ a_gamma.date.Split(';')[1];
            distanceText.text = a_gamma.distance;
            textPanel.SetActive(true);
            yield return new WaitForSeconds(7f);
            the_gamma.SetActive(false);
            textPanel.SetActive(false);
        }
        yield return null;
    }
}
