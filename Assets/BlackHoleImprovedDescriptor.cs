﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlackHoleImprovedDescriptor : MonoBehaviour {

	public float delay, stoptime;
    private float time, scale, whiteScale, blastTime, outerAlpha;
    private bool forceVanished, compared, blastStarted, rayShown;

    public GameObject[] fermiGravity;
    public GameObject description, comparisonText, blastRay, mainStar,
                blackhole, blackholeGas, whiteParticle, cinemachineHolder, mainPanel;

    Text descriptionText;
    private string[] blackHoleTexts = {
        "When a star has the mass 3 times greater than the sun, at its final stage gravitational force pushes its gas to the center"+
        " while fermi pressure and some other forces push it outside",
        "As long as both the forces are same the star stays in a stable state",
        "When the gravitational force is greater than fermi pressure it squeezes the star to its center",
        "When squeezed at the end of its life the star sometimes creates a big blast which sometimes creates a blackhole",
        "A black hole is a region of spacetime exhibiting strong gravitational effects.",
        "Nothing, not even particles and electromagnetic radiation such as light can escape from inside it.",
        "The theory of general relativity predicts that a sufficiently compact mass can deform spacetime to form a black hole.",
        "At the center of a black hole, lies a gravitational singularity, a region where the spacetime curvature becomes infinite.",
        "For a non-rotating black hole, this region takes the shape of a single point.",
        "For a rotating black hole, it is smeared out to form a ring singularity that lies in the plane of rotation",
        " "
    };


    void Start()
    {
        descriptionText = description.GetComponent<Text>();
        scale = mainStar.transform.localScale.x;
        whiteScale = whiteParticle.transform.localScale.x;
        StartCoroutine(SholwBlackholeDescription());
    }

    void Update()
    {
        time += Time.deltaTime;

        if (blastStarted)
        {
            blastTime += Time.deltaTime;
            //Debug.Log(blastTime);
        }

        if (compared && scale > 0.8f)
        {
            scale = mainStar.transform.localScale.x;
            scale -= 0.001f;
            mainStar.transform.localScale = new Vector3(scale, scale, scale);
        }
        if (compared && whiteScale > 0.6f)
        {
            whiteScale = whiteParticle.transform.localScale.x;
            whiteScale -= 0.001f;
            whiteParticle.transform.localScale = new Vector3(whiteScale, whiteScale, whiteScale);
        }

        if (blastTime > 6)
        {
            blackhole.SetActive(true);
            rayShown = true;
        }

        if (blastTime > 8)
        {
            cinemachineHolder.SetActive(true);
        }

        if (rayShown)
        {
            outerAlpha += (outerAlpha >= 1) ? 0 : 0.004f;
            outerAlpha = (outerAlpha > 1) ? 1 : outerAlpha;
        }

    }

    IEnumerator SholwBlackholeDescription()
    {
        for (int i = 0; i < blackHoleTexts.Length; i++)
        {

            for (int j = 0; j < blackHoleTexts[i].Length; j++)
            {
                descriptionText.text = blackHoleTexts[i].Substring(0, j + 1);
                if (i == 0 && j == 74)
                {
                    fermiGravity[2].SetActive(true);
                    fermiGravity[3].SetActive(true);
                }
                if (i == 0 && j == 132)
                {
                    fermiGravity[0].SetActive(true);
                    fermiGravity[1].SetActive(true);
                }
                if (i == 2 && !forceVanished)
                {
                    fermiGravity[0].SetActive(false);
                    fermiGravity[1].SetActive(false);
                    fermiGravity[2].SetActive(false);
                    fermiGravity[3].SetActive(false);
                }
                if (i == 2 && j == 8 && !compared)
                {
                    compared = true;
                }
                if (i == 2 && j == 11)
                {
                    comparisonText.SetActive(true);
                }
                if (i == 3)
                {
                    comparisonText.SetActive(false);
                }
                if (i == 3 && j == 66)
                {
                    blastRay.SetActive(true);
                    mainStar.SetActive(false);
                    blastStarted = true;

                }
                if (i == 7 && j == 52)
                {
                    fermiGravity[4].SetActive(true);
                    fermiGravity[5].SetActive(true);
                }
                if (i == 8 && j == 0)
                {
                    fermiGravity[4].SetActive(false);
                    fermiGravity[5].SetActive(false);
                    fermiGravity[6].SetActive(true);
                    fermiGravity[7].SetActive(true);
                }
                if (i == 8 && j == 50)
                {
                    fermiGravity[6].SetActive(false);
                    fermiGravity[7].SetActive(false);
                    fermiGravity[8].SetActive(true);
                    fermiGravity[9].SetActive(true);
                }
                if (i == 9 && j == 20)
                {
                    fermiGravity[8].SetActive(false);
                    fermiGravity[9].SetActive(false);
                    fermiGravity[10].SetActive(true);
                    fermiGravity[11].SetActive(true);
                }
                if (i == 10)
                {
                    mainPanel.SetActive(false);
                    fermiGravity[10].SetActive(false);
                    fermiGravity[11].SetActive(false);
                }
                yield return new WaitForSeconds(delay);
            }
            yield return new WaitForSeconds(stoptime);
        }
        yield return null;
    }

}
