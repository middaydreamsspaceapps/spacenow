using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBlinkEffector : MonoBehaviour {

	public float totalTime;
	private float currentTime;
    [SerializeField]
	Color buttonColor;

	bool decreasing, ended;

	private float alpha = 0f;

	public float highestAlpha, lowestAlpha, increaseRate;

	void Awake ()
	{
        //Debug.Log(GetComponent<Image>().color.ToString() + "  " + buttonColor.ToString());
	}


	void Update ()
	{
		currentTime += Time.deltaTime;
	}

	private bool EffectRunning = false;
	public void StartEffect ()
	{
		currentTime = 0;
		ended = false;
		if (EffectRunning) return;
		EffectRunning = true;
		StartCoroutine (GlowButton ());
	}

	public void StopEffect ()
	{
		StopAllCoroutines ();
        buttonColor = GetComponent<Image>().color;
		GetComponent<Image> ().color = new Color (buttonColor.r, buttonColor.g, buttonColor.b, 1);
		EffectRunning = false;
	}

	IEnumerator GlowButton ()
	{
        bool originalColorFound = false;
		while (true) {
            /*if( !originalColorFound)
            {
                yield return new WaitForSeconds(1);
                buttonColor = GetComponent<Image>().color;
                originalColorFound = true;
            }*/
			if (ended && alpha >= lowestAlpha) {
				alpha -= increaseRate;
			}
			if (currentTime >= totalTime) {
				ended = true;
			}

			if (ended && alpha <= lowestAlpha) {
				EffectRunning = false;
				break;
			}


			if (!ended) {
				if (!decreasing && alpha <= highestAlpha) {
					alpha += increaseRate;
				} else if (decreasing && alpha >= lowestAlpha) {
					alpha -= increaseRate;
				}

				if (alpha >= highestAlpha) {
					decreasing = true;
				} else if (alpha <= lowestAlpha) {
					decreasing = false;
				}
			}
            buttonColor = GetComponent<Image>().color;
			GetComponent<Image> ().color = new Color (buttonColor.r, buttonColor.g, buttonColor.b, alpha);
			yield return new WaitForSeconds (0.005f);
		}
		yield return null;
	}
}
