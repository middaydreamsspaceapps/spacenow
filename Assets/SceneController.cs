﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public GameObject fadeoutobj;
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {

        Application.targetFrameRate = 60;
      //  StartCoroutine(LoadScene());
        //StartCoroutine(Timer());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(30);
        fadeoutobj.SetActive(true);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Fermi working Principle Scene");
    }

    IEnumerator Timer()
    {
        yield return null;
        while (true)
        {
            Debug.Log(Time.time);
            yield return new WaitForSeconds(1);
        }
    }
}
