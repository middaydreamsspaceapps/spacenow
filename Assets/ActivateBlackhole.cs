﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateBlackhole : MonoBehaviour
{
    public GameObject blackhole;
    
    void Start()
    {
        StartCoroutine(ShowBlackhole());
    }

    void Update()
    {
        
    }

    IEnumerator ShowBlackhole()
    {
        yield return new WaitForSeconds(7.5f);
        blackhole.SetActive(true);
        yield return null;
    }

}
