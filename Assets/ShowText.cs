﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowText : MonoBehaviour
{
    public GameObject blackholeText;

    void Start()
    {
        StartCoroutine(ShowBlackholeText());
    }

    void Update()
    {
        
    }

    IEnumerator ShowBlackholeText()
    {
        yield return new WaitForSeconds(3.5f);
        blackholeText.SetActive(true);
        yield return null;
    }
}
