﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class SkyboxController : MonoBehaviour
{
    public GameObject TextPanel;
    public Text SolarEclipseText;
    Material SkyViewSky_Mat;
    Material GroundViewSky_Mat;
    public GameObject SkyViewObjects, GroundViewObjects, IntervalPanel;
    public string SkyViewMatName, GroundViewMatName;

    // Use this for initialization
    void Start()
    {
        SkyViewSky_Mat = Resources.Load(SkyViewMatName, typeof(Material)) as Material;
        GroundViewSky_Mat = Resources.Load(GroundViewMatName, typeof(Material)) as Material;
        TextPanel.SetActive(false);
        // StartCoroutine(RepeatScene());
        StartCoroutine(TextSetter());
        StartCoroutine(ViewChanger());
    }
    IEnumerator TextSetter()
    {
        yield return new WaitForSeconds(4);
        TextPanel.SetActive(true);
        SolarEclipseText.text = "A solar eclipse occurs when an observer on Earth passes through the shadow cast by the Moon which fully or partially blocks the Sun.";
        yield return new WaitForSeconds(7);
        TextPanel.SetActive(false);
        SolarEclipseText.text = "";
        yield return new WaitForSeconds(2);
        TextPanel.SetActive(true);
        SolarEclipseText.text = "Solar eclipse happens when the earth, moon, and sun are on the same plane.There are four types of solar eclipses-total, annular, hybrid and partial eclipse.";
        yield return new WaitForSeconds(8);
        TextPanel.SetActive(false);
        SolarEclipseText.text = "";

    }

    [SerializeField]
    Animator IntervalPanelAnimtor;
    IEnumerator ViewChanger()
    {
        yield return new WaitForSeconds(34);
        RenderSettings.skybox = SkyViewSky_Mat;
        IntervalPanel.SetActive(true);
        yield return new WaitForSeconds(1);
        GroundViewObjects.SetActive(false);
        yield return new WaitForSeconds(1.5f);
        IntervalPanelAnimtor.SetBool("FadeOut", true);
        yield return new WaitForSeconds(2f);
        IntervalPanel.SetActive(false);
        SkyViewObjects.SetActive(true);
    }
    //IEnumerator RepeatScene()
    //{
    //    yield return new WaitForSeconds(85);
    //    SceneManager.LoadSceneAsync(36);
    //}
}