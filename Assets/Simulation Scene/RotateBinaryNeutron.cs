using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateBinaryNeutron : MonoBehaviour {

    public GameObject anotherSphere, center, blackHole, rayOne, 
            thisLight, anotherLight, upperParticle, lowerParticle, theWave, anotherWave;
    public float speed, lifetime, currentTime;
    private float xPosition, anotherX, startTime;
    public ParticleSystem spread;
    bool appeared;
    
    void Start () {
        StartCoroutine(StartSupernova());
	}

	void Update () {
                
    }

    IEnumerator StartSupernova()
    {
        float newTime = 0f, spriteAlpha = 0.686f;
        bool isMerged = false, burst = false;
        while (true)
        {
            currentTime += 0.01f;
            xPosition = transform.position.x;
            anotherX = anotherSphere.transform.position.x;
            speed += 1.6f;
            xPosition += (xPosition < 0) ? 1f : 0;
            transform.position = new Vector3(xPosition, transform.position.y, transform.position.z);
            anotherX -= (anotherX > 0) ? 1f : 0;
            anotherSphere.transform.position = new Vector3(anotherX, anotherSphere.transform.position.y,
            anotherSphere.transform.position.z);

            lifetime -= (lifetime < 0) ? 0 : 0.005f;
            lifetime = (lifetime < 0) ? 0 : lifetime;

            var mainParticle = spread.main;
            mainParticle.startLifetime = lifetime;

            if (anotherX < 0)
            {
                anotherX = (-1) * anotherX;
            }

            if (isMerged)
            {
                newTime += 0.01f;
            }

            if (newTime > 2f & !burst)
            {
                rayOne.SetActive(true);
                thisLight.SetActive(false);
                anotherLight.SetActive(false);
                upperParticle.SetActive(false);
                lowerParticle.SetActive(false);
                //blackHole.SetActive(true);
                burst = true;
            }

            if (anotherX <= 0.04f || isMerged)
            {
                isMerged = true;
                //theWave.SetActive(false);
                //anotherWave.SetActive(false);
                Color spriteColor = theWave.GetComponent<SpriteRenderer>().color;
                spriteAlpha -= 0.0054f;
                spriteColor.a = spriteAlpha;
                theWave.GetComponent<SpriteRenderer>().color = spriteColor;
                spriteColor = anotherWave.GetComponent<SpriteRenderer>().color;
                spriteColor.a = spriteAlpha;
                anotherWave.GetComponent<SpriteRenderer>().color = spriteColor;
                if (!burst)
                {
                    upperParticle.SetActive(true);
                    lowerParticle.SetActive(true);
                }
            }
            transform.RotateAround(center.transform.position, -center.transform.up, speed * Time.deltaTime);
            anotherSphere.transform.RotateAround(center.transform.position, -center.transform.up, speed * Time.deltaTime);

            yield return new WaitForSeconds(0.01f);
        }
        yield return null;
    }
}

