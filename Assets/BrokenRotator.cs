﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenRotator : MonoBehaviour {

	public GameObject referenceCube;
	public float speed;
	void Start () {
		
	}
	
	void Update () {
		gameObject.transform.RotateAround(referenceCube.transform.position, Vector3.up, speed);
	}
}
