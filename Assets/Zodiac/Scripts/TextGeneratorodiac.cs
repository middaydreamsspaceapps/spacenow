﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextGeneratorodiac : MonoBehaviour {
	public Text ZodiacSysDescriptionText;
	public GameObject TextPanel;
	// Use this for initialization
	void Start ()
	{
		TextPanel.SetActive (false);
		StartCoroutine (TextSetter ());

	}


	IEnumerator TextSetter ()
	{
		yield return new WaitForSeconds (1);
		TextPanel.SetActive (true);
		ZodiacSysDescriptionText.text = "The zodiac is an area of the sky that extends approximately 8° north or south (as measured in celestial latitude) of the ecliptic.";
		yield return new WaitForSeconds (5);
		TextPanel.SetActive (false);
		ZodiacSysDescriptionText.text = "";
		yield return new WaitForSeconds (1);
		TextPanel.SetActive (true);
		ZodiacSysDescriptionText.text = "It is the apparent path of the sun across the celestial sphere over the course of the year. The path of moon and visible planets are also within the belt of the zodiac.";
		yield return new WaitForSeconds (5);
		TextPanel.SetActive (false);
		ZodiacSysDescriptionText.text = "";
		yield return new WaitForSeconds (6);
		TextPanel.SetActive (true);
		ZodiacSysDescriptionText.text = "The zodiac is divided into 12 signs each occupying 30° of celestial longitude.";
		yield return new WaitForSeconds (4);
		TextPanel.SetActive (false);
		ZodiacSysDescriptionText.text = "";
		yield return new WaitForSeconds (1);
		TextPanel.SetActive (true);
		ZodiacSysDescriptionText.text = "The division of ecliptic into zodiac sign was originated in Babylonian astronomy during the first half of the 1st millennium BC.";
		yield return new WaitForSeconds (5);
		TextPanel.SetActive (false);
		ZodiacSysDescriptionText.text = "";
		yield return new WaitForSeconds (4);
		TextPanel.SetActive (true);
		ZodiacSysDescriptionText.text = "Constellations like Gemini and Cancer can be traced back further to bronze age.";
		yield return new WaitForSeconds (4);
		TextPanel.SetActive (false);
		ZodiacSysDescriptionText.text = "";
		yield return new WaitForSeconds (6);
		TextPanel.SetActive (true);
		ZodiacSysDescriptionText.text = "Babylonian astronomers divided the ecliptic into 12 equal signs. Each sign contained 30° of celestial longitude. Zodiac was the first known celestial coordinate system.";
		yield return new WaitForSeconds (6);
		TextPanel.SetActive (false);
		ZodiacSysDescriptionText.text = "";
		yield return new WaitForSeconds (3);
		TextPanel.SetActive (true);
		ZodiacSysDescriptionText.text = "In ancient time, astronomers didn’t fully understand how sun, earth and the star move. So, their prediction on zodiac was not totally correct.";
		yield return new WaitForSeconds (6);
		TextPanel.SetActive (false);
		ZodiacSysDescriptionText.text = "";
		yield return new WaitForSeconds (1);
		TextPanel.SetActive (true);
		ZodiacSysDescriptionText.text = "In 2011, astronomers from Minnesota Planetarium society found that, due to Moon’s gravitational pull on earth, the alignment of stars has been shifted. As Earth’s axis changed direction, the mathematics behind calculation of zodiac sign is changed.";
		yield return new WaitForSeconds (12);
		TextPanel.SetActive (false);
		ZodiacSysDescriptionText.text = "";
		yield return new WaitForSeconds (1);
		TextPanel.SetActive (true);
		ZodiacSysDescriptionText.text = "Ophiuchus is declared as 13th zodiac sign in recent years.";
		yield return new WaitForSeconds (4);
		TextPanel.SetActive (false);
		ZodiacSysDescriptionText.text = "";



	}
}