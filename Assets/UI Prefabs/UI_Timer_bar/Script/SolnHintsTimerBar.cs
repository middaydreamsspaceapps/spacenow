﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SolnHintsTimerBar : MonoBehaviour {

	public static SolnHintsTimerBar instance;
	Image fillImg;

	void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
		fillImg = this.GetComponent<Image> ();
	}
	void Start ()
	{

		//StartCoroutine(ProgressBar(10));
	}

	IEnumerator ProgressBar (float time)
	{
		//Debug.Log("Time to Progress = " + time);
		float TimeToFill = time;
		fillImg.fillAmount = 1; //reset initially to 100%
		float ProgressRate = 1 / TimeToFill;
		while (time > 0) {
			time -= 0.1f;
			fillImg.fillAmount = time / TimeToFill;
			//Debug.Log("Time to fill = " + time + " fiil amout = " + fillImg.fillAmount);
			yield return new WaitForSeconds (0.1f);
		}
	}

	public void TimerBarStart (float time)
	{
		StopAllCoroutines ();
		StartCoroutine (ProgressBar (time));
	}
	public void TimerBarReset ()
	{
		fillImg.fillAmount = 1;
	}

	public void HideTimerBar (bool Hide)
	{
		Color c;
		if (Hide) {
			c = fillImg.color;
			c.a = 0;
			fillImg.color = c;

			c = transform.parent.gameObject.GetComponent<Image> ().color;
			c.a = 0;
			transform.parent.gameObject.GetComponent<Image> ().color = c;
		} else {
			c = fillImg.color;
			c.a = 1;
			fillImg.color = c;

			c = transform.parent.gameObject.GetComponent<Image> ().color;
			c.a = 1;
			transform.parent.gameObject.GetComponent<Image> ().color = c;
		}
	}

}
