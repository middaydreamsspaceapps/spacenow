﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PulsarText : MonoBehaviour {

    public float delay;
    private string updateText;
    private string[] pulsarTexts = { "Pulsar, in astronomy, is a neutron star that emits brief, sharp pulses of energy instead of the steady radiation associated with other natural sources",
    "In 1974, Joseph Hooton Taylor, Jr.and Russell Hulse discovered for the first time a pulsar in a binary system, PSR B1913+16",
    "A binary star is a star system consisting of two stars orbiting around their common barycenter. Systems of two or more stars are called multiple star systems"};
    
    void Start () {
        StartCoroutine(ShowPulsarText());
    }
	
	IEnumerator ShowPulsarText()
    {
        for(int j=0; j < pulsarTexts.Length; j++)
        {
            yield return new WaitForSeconds(4);
            for (int i = 0; i < pulsarTexts[j].Length; i++)
            {
                updateText = pulsarTexts[j].Substring(0, i+1);
                GetComponent<Text>().text = updateText;
                yield return new WaitForSeconds(delay);
            }
        }
        yield return new WaitForSeconds(4);
        gameObject.SetActive(false);
        yield return null;
    }
}
