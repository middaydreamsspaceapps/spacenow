﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CustomSceneLooder : MonoBehaviour
{
    public GameObject fadeoutobj;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadScene());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(4);
        fadeoutobj.SetActive(true);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("Fermi working Principle Scene");
    }
}
