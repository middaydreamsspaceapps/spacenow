﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInStar : MonoBehaviour {

    public float currentTime = 0;
    public GameObject[] gameObjects;

    void Start()
    {

    }


    void Update()
    {
        currentTime += Time.deltaTime;

        if (currentTime >= 2)
        {
            gameObjects[11].SetActive(true);
            
        }
        if (currentTime >= 4)
        {
            gameObjects[10].SetActive(true);
        }
        if (currentTime >= 7)
        {
            gameObjects[9].SetActive(true);
        }
        if (currentTime >= 9)
        {
            gameObjects[8].SetActive(true);
        }
        if (currentTime >= 12)
        {
            gameObjects[7].SetActive(true);
            gameObjects[6].SetActive(true);
        }
        if (currentTime >= 15)
        {
            gameObjects[5].SetActive(true);
        }
        if (currentTime >= 17)
        {
            gameObjects[4].SetActive(true);
        }
        if (currentTime >= 20)
        {
            gameObjects[3].SetActive(true);
        }
        if (currentTime >= 23)
        {
            gameObjects[2].SetActive(true);
        }
        if (currentTime >= 25)
        {
            gameObjects[1].SetActive(true);
            gameObjects[0].SetActive(true);
        }
        if(currentTime >= 29)
        {
            gameObjects[12].SetActive(true);
        }
        
    }
}
