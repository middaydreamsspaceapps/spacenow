﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecondSupernova : MonoBehaviour {

    public float delay, waitBetween;
    private string updateText;
    private string[] pulsarTexts = {
        "A supernova is an astronomical event that occurs during the last stellar evolutionary stages of a massive star's life",
        "This causes the sudden appearance of a new bright star, before slowly fading from sight over several weeks or months",
        "When most of a star's fuel is burnt a star can expand upto around 10 times its current size and 50 times its luminosity, with a temperature a little lower than normal",
        "At this stage it is called a red giant",
        "In the next stage the nuclear reaction inside the red giant detonates itself",
        "What remains after the explosion is a Nebula"};

    void Start()
    {
        StartCoroutine(ShowSunText());
    }

    IEnumerator ShowSunText()
    {
        for (int j = 0; j < pulsarTexts.Length; j++)
        {
                yield return new WaitForSeconds(waitBetween);
                if(j==5){
                    GetComponent<Text>().text = "";
                    yield return new WaitForSeconds(8);
                }
            for (int i = 0; i < pulsarTexts[j].Length; i++)
            {
                updateText = pulsarTexts[j].Substring(0, i + 1);
                GetComponent<Text>().text = updateText;
                yield return new WaitForSeconds(delay);
            }
        }
        yield return new WaitForSeconds(waitBetween);
        gameObject.SetActive(false);
        yield return null;
    }
}
