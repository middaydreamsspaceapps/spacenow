﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SunCycleText : MonoBehaviour {

    public float delay, waitBetween;
    private string updateText;
    private string[] pulsarTexts = { "According to Nebular Theory, the Sun and all the planets of our Solar System began as a giant cloud of molecular gas and dust",
        "Then, about 4.57 billion years ago, the cloud began to collapse",
        "Most of the material ended up in a ball at the center while the rest of the matter flattened out into disk that circled around it",
        "4.8 billion years from now the core of the sun will ignite violently in a helium flash",
        "As a result, approximately 6% of the core and 40% of the Sun’s mass will be converted into carbon within a matter of minute",
        "The Sun will then expand upto around 10 times its current size and 50 times its luminosity, with a temperature a little lower than today",
        "At this time it will be called a red giant",
        "When it runs out of fuel, it collapses inward on itself","It will contain approximately the mass of the whole sun but have roughly the radius of Earth","This makes them incredibly dense, beaten out only by neutron stars and black holes",
        "At this stage this is called a white dwarf",
        "The gravity on the surface of a white dwarf is 350,000 times that of gravity on Earth"};

    void Start()
    {
        StartCoroutine(ShowSunText());
    }

    IEnumerator ShowSunText()
    {
        for (int j = 0; j < pulsarTexts.Length; j++)
        {
            if (j == 2)
            {
                yield return new WaitForSeconds(1.8f);
            }
            else if (j == 7)
            {
                yield return new WaitForSeconds(5f);
            }
            else
            {
                yield return new WaitForSeconds(waitBetween);
            }
            for (int i = 0; i < pulsarTexts[j].Length; i++)
            {
                updateText = pulsarTexts[j].Substring(0, i + 1);
                GetComponent<Text>().text = updateText;
                yield return new WaitForSeconds(delay);
            }
        }
        yield return new WaitForSeconds(waitBetween);
        gameObject.SetActive(false);
        yield return null;
    }
}
