﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInSupernova2 : MonoBehaviour {

    public float currentTime = 0;
    public GameObject[] gameObjects;
    public GameObject nebula, center, redGiantText, nebulaText;
    
    //nebulaIntroText;
    public float speed;
    bool done, vanished, redgiant, aNebula, nebulaIntro;

    void Start()
    {

    }
    
    void Update()
    {
        currentTime += Time.deltaTime;

        Debug.Log(currentTime);

        if (!done)
        {
            if (currentTime >= 4)
            {
                gameObjects[4].SetActive(true);
            }
            if (currentTime >= 6)
            {
                gameObjects[5].SetActive(true);
            }
            if(currentTime >= 8)
            {
                gameObjects[6].SetActive(true);
            }
            if(currentTime >= 10)
            {
                gameObjects[7].SetActive(true);
            }
            if(currentTime >= 12)
            {
                gameObjects[8].SetActive(true);
                done = true;
            }
        }
        else if(!vanished)
        {
            if(currentTime >= 14)
            {
                gameObjects[0].SetActive(false);
            }
            if (currentTime >= 16)
            {
                gameObjects[1].SetActive(false);
            }
            if (currentTime >= 18)
            {
                gameObjects[2].SetActive(false);
            }
            if (currentTime >= 20)
            {
                gameObjects[3].SetActive(false);
            }
        }

        
        if (currentTime >= 17)
        {
            for(int i=4; i<=8; i++)
            {
                float x = gameObjects[i].transform.localScale.x;
                float y = gameObjects[i].transform.localScale.y;
                float z = gameObjects[i].transform.localScale.z;
                if (i==4)
                {
                    x += (x > 5) ? 0 : 0.02f;
                    y += (y > 5) ? 0 : 0.02f;
                    z += (z > 5) ? 0 : 0.02f;
                }
                else
                {
                    x += (x > 100) ? 0 : 0.5f;
                    y += (y > 100) ? 0 : 0.5f;
                    z += (z > 100) ? 0 : 0.5f;
                }
                gameObjects[i].transform.localScale = new Vector3(x, y, z);
            }
        }

        if(currentTime>=24 && !redgiant)
        {
            redGiantText.SetActive(true);
            redgiant = true;
        }
        if (currentTime >= 35.5)
        {
            redGiantText.SetActive(false);
        }

        if (currentTime >= 37)
        {
            gameObjects[9].SetActive(true);
        }
        if(currentTime>=42){
            gameObjects[4].SetActive(false);
            gameObjects[5].SetActive(false);
            gameObjects[6].SetActive(false);
            gameObjects[7].SetActive(false);
            gameObjects[8].SetActive(false);
        }
        if (currentTime >= 43)
        {
            nebula.SetActive(true);
            nebula.transform.RotateAround(center.transform.position, -center.transform.up, speed * Time.deltaTime);
        }
        if (currentTime >= 54 && !aNebula)
        {
            aNebula = true;
        }
        if (currentTime >= 57)
        {
            nebulaText.SetActive(false);
        }
    }
}

