﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeMysun : MonoBehaviour {

	 public float currentTime = 0;
    public GameObject[] gameObjects;
    bool done, redGiant, resized;

    void Start()
    {

    }
    
    void Update()
    {
        currentTime += Time.deltaTime;

        Debug.Log(currentTime);

		if(!done){
				if(currentTime>1){
				gameObjects[0].SetActive(true);
			}
			if(currentTime>2.5){
				gameObjects[1].SetActive(true);
			}
			if(currentTime>3.5){
				gameObjects[2].SetActive(true);
			}
			if(currentTime>4.5){
				gameObjects[3].SetActive(true);
			}
			if(currentTime>6.5){
				gameObjects[4].SetActive(true);
			}
			if(currentTime>18){
				gameObjects[5].SetActive(true);
			}
			if(currentTime>20){
				gameObjects[6].SetActive(true);
			}
			if(currentTime>22){
				gameObjects[7].SetActive(true);
				gameObjects[0].SetActive(false);
			}
			if(currentTime>25){
				gameObjects[8].SetActive(true);
				gameObjects[1].SetActive(false);
			}
			if(currentTime>27){
				gameObjects[9].SetActive(true);
				gameObjects[2].SetActive(false);
			}
			if(currentTime>29){
				gameObjects[3].SetActive(false);
			}
			if(currentTime>31){
				gameObjects[4].SetActive(false);
			}
			if(currentTime>33){
				gameObjects[5].SetActive(false);
				done = true;
			}
		}

		if(!redGiant){
			if(currentTime>41){
				gameObjects[14].SetActive(true);
			}
			if(currentTime>42){
				gameObjects[5].SetActive(false);
			}
			if(currentTime>43){
				gameObjects[13].SetActive(true);
			}
			if(currentTime>44){
				gameObjects[6].SetActive(false);
			}
			if(currentTime>45){
				gameObjects[12].SetActive(true);
			}
			if(currentTime>46){
				gameObjects[7].SetActive(false);
			}
			if(currentTime>47){
				gameObjects[11].SetActive(true);
			}
			if(currentTime>48){
				gameObjects[8].SetActive(false);
			}
			if(currentTime>49){
				gameObjects[10].SetActive(true);
			}
			if(currentTime>50){
				gameObjects[9].SetActive(false);
				redGiant = true;
			}
		}

		if(currentTime>62){
				gameObjects[15].SetActive(true);
			}
			if(currentTime>63){
				gameObjects[10].SetActive(false);
			}
			if(currentTime>64){
				gameObjects[16].SetActive(true);
			}
			if(currentTime>65){
				gameObjects[11].SetActive(false);
			}
			if(currentTime>66){
				gameObjects[17].SetActive(true);
			}
			if(currentTime>67){
				gameObjects[12].SetActive(false);
			}
			if(currentTime>68){
				gameObjects[18].SetActive(true);
			}
			if(currentTime>69){
				gameObjects[13].SetActive(false);
			}
			if(currentTime>70){
				gameObjects[19].SetActive(true);
			}
			if(currentTime>71){
				gameObjects[14].SetActive(false);
				redGiant = true;
			}

        if (currentTime >= 49 && !resized)
        {
            for(int i=10; i<=19; i++)
            {
                float x = gameObjects[i].transform.localScale.x;
                float y = gameObjects[i].transform.localScale.y;
                float z = gameObjects[i].transform.localScale.z;
                if (i==10 || i == 15)
                {
                    x += (x > 5) ? 0 : 0.02f;
                    y += (y > 5) ? 0 : 0.02f;
                    z += (z > 5) ? 0 : 0.02f;
                }
                else
                {
                    x += (x > 100) ? 0 : 0.5f;
                    y += (y > 100) ? 0 : 0.5f;
                    z += (z > 100) ? 0 : 0.5f;

					if(z==100){
						resized = true;
					}
                }
                gameObjects[i].transform.localScale = new Vector3(x, y, z);
            }
        }
        if (currentTime >= 62)
        {
            for (int i = 10; i <= 19; i++)
            {
                float x = gameObjects[i].transform.localScale.x;
                float y = gameObjects[i].transform.localScale.y;
                float z = gameObjects[i].transform.localScale.z;
                if (i == 10 || i ==15)
                {
                    x -= (x < 1) ? 0 : 0.015f;
                    y -= (y < 1) ? 0 : 0.015f;
                    z -= (z < 1) ? 0 : 0.015f;
                }
                else
                {
                    x -= (x < 20) ? 0 : 0.30f;
                    y -= (y < 20) ? 0 : 0.30f;
                    z -= (z < 20) ? 0 : 0.30f;
                }
                gameObjects[i].transform.localScale = new Vector3(x, y, z);
            }
        }
        
    }
}
