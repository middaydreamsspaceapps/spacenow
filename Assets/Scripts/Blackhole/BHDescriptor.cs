﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BHDescriptor : MonoBehaviour {

    public float delay;
    private string updateText;
    private string[] pulsarTexts = { "A black hole is a region of spacetime exhibiting strong gravitational effects",
    "Nothing, not even particles and electromagnetic radiation such as light—can escape from inside it",
    "The theory of general relativity predicts that a sufficiently compact mass can deform spacetime to form a black hole"};

    void Start()
    {
        StartCoroutine(ShowPulsarText());
    }

    IEnumerator ShowPulsarText()
    {
        for (int j = 0; j < pulsarTexts.Length; j++)
        {
            yield return new WaitForSeconds(4);
            for (int i = 0; i < pulsarTexts[j].Length; i++)
            {
                updateText = pulsarTexts[j].Substring(0, i + 1);
                GetComponent<Text>().text = updateText;
                yield return new WaitForSeconds(delay);
            }
        }
        yield return new WaitForSeconds(4);
        gameObject.SetActive(false);
        yield return null;
    }
}
