﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddTextToSuperNova : MonoBehaviour
{

    public float delay, waitBetween;
    private string updateText;
    private string[] pulsarTexts = {
        "A binary star is a star system consisting of two stars orbiting around their common barycenter",
        "These systems, especially when more distant, often appear to the unaided eye as a single point of light",
        "When two binary stars comes close enough they turn into a single one and cause a massive explosion called supernova",
        "The earliest supernova to be observed occurred in 185 AD.\nIt was recorded by Chinese astronomers",
        "For a short period of time, a single supernova can\neasily outshine an entire galaxy of stars" };

    void Start()
    {
        StartCoroutine(ShowSunText());
    }

    IEnumerator ShowSunText()
    {
        for (int j = 0; j < pulsarTexts.Length; j++)
        {
            if (j == 2)
            {
                yield return new WaitForSeconds(1.8f);
            }
            else if (j == 7)
            {
                yield return new WaitForSeconds(5f);
            }
            else
            {
                yield return new WaitForSeconds(waitBetween);
            }
            for (int i = 0; i < pulsarTexts[j].Length; i++)
            {
                updateText = pulsarTexts[j].Substring(0, i + 1);
                GetComponent<Text>().text = updateText;
                yield return new WaitForSeconds(delay);
            }
        }
        yield return new WaitForSeconds(waitBetween);
        gameObject.SetActive(false);
        yield return null;
    }
}
