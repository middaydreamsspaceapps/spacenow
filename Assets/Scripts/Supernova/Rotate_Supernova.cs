﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rotate_Supernova : MonoBehaviour {

    public GameObject anotherSphere, center, blackHole, rayOne, thisLight, anotherLight;
    public float speed, lifetime, currentTime;
    private float xPosition, anotherX, startTime;
    public ParticleSystem spread;
    bool appeared;
    
    void Start () {

	}

	void Update () {

        currentTime += Time.deltaTime;

        xPosition = transform.position.x;
        anotherX = anotherSphere.transform.position.x;
        speed += 0.8f;
        xPosition += (xPosition < 0) ? 0.7f : 0;
        transform.position = new Vector3(xPosition, transform.position.y, transform.position.z);
        anotherX -= (anotherX > 0) ? 0.7f : 0;
        anotherSphere.transform.position = new Vector3(anotherX, anotherSphere.transform.position.y,
        anotherSphere.transform.position.z);
        
        lifetime -=(lifetime<0)?0: 0.0039f;
        lifetime = (lifetime<0)?0:lifetime;
        
        var mainParticle = spread.main;
        mainParticle.startLifetime = lifetime;

        transform.RotateAround(center.transform.position,  -center.transform.up, speed * Time.deltaTime);
        anotherSphere.transform.RotateAround(center.transform.position, -center.transform.up, speed * Time.deltaTime);

        float thisX = gameObject.transform.localScale.x;
        float thisY = gameObject.transform.localScale.y;
        float thisZ = gameObject.transform.localScale.z;

        thisX += (thisX>240)?0:0.08f;
        thisY += (thisY>240)?0:0.08f;
        thisZ += (thisZ>240)?0:0.08f;

        gameObject.transform.localScale = new Vector3(thisX, thisY, thisZ);


        float thisLightX = thisLight.transform.localScale.x;
        float thisLightY = thisLight.transform.localScale.y;
        float thisLightZ = thisLight.transform.localScale.z;

        thisLightX += (thisLightX>100)?0:0.027f;
        thisLightY += (thisLightY>100)?0:0.027f;
        thisLightZ += (thisLightZ>100)?0:0.027f;

        thisLight.transform.localScale = new Vector3(thisLightX, thisLightY, thisLightZ);

        
        float anotherSphereX = anotherSphere.transform.localScale.x;
        float anotherSphereY = anotherSphere.transform.localScale.y;
        float anotherSphereZ = anotherSphere.transform.localScale.z;

        anotherSphereX -= (anotherSphereX<90)?0:0.04f;
        anotherSphereY -= (anotherSphereY<90)?0:0.04f;
        anotherSphereZ -= (anotherSphereZ<90)?0:0.04f;

        anotherSphere.transform.localScale = new Vector3(anotherSphereX, anotherSphereY, anotherSphereZ);


        float anotherLightX = anotherLight.transform.localScale.x;
        float anotherLightY = anotherLight.transform.localScale.y;
        float anotherLightZ = anotherLight.transform.localScale.z;

        anotherLightX -= (anotherLightX<30)?0:0.027f;
        anotherLightY -= (anotherLightY<30)?0:0.027f;
        anotherLightZ -= (anotherLightZ<30)?0:0.027f;

        anotherLight.transform.localScale = new Vector3(anotherLightX, anotherLightY, anotherLightZ);
        
        if (currentTime >=28)
        {
            startTime += Time.deltaTime;
        }

        if (startTime > 2)
        {
            rayOne.SetActive(true);
        }

        if (startTime > 10)
        {
            blackHole.SetActive(true);
            anotherSphere.SetActive(false);
            gameObject.SetActive(false);
        }
        if(currentTime>27){
            anotherSphere.SetActive(false);
        }
    }
}

