﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuasarRotator : MonoBehaviour {

    public List<GameObject> gameObjects;
    public float speed;


	void Start () {
		
	}
	

	void Update () {
        transform.Rotate(0,-Time.deltaTime*speed,0,Space.World);
        gameObjects[0].transform.Rotate(0, -Time.deltaTime * speed, 0, Space.World);
        gameObjects[1].transform.Rotate(0, -Time.deltaTime * speed, 0, Space.World);
        gameObjects[2].transform.Rotate(0, -Time.deltaTime * speed, 0, Space.World);
        gameObjects[3].transform.Rotate(0, -Time.deltaTime * speed, 0, Space.World);
        gameObjects[4].transform.Rotate(0, -Time.deltaTime * speed, 0, Space.World);
        gameObjects[5].transform.Rotate(0, -Time.deltaTime * speed, 0, Space.World);
        gameObjects[6].transform.Rotate(0, -Time.deltaTime * speed, 0, Space.World);
    }
}
