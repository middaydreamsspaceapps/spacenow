﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeG : MonoBehaviour {


    private float currentTime;
    Text text;
    Color originalColor;
    float a;

    bool vanished, done;

    private void Start()
    {
        text = GetComponent<Text>();
        originalColor = text.color;
    }

    private void Update()
    {
        if (currentTime == 0)
        {
            text.color = new Color(originalColor.r, originalColor.g, originalColor.b,
                0.0f);
            vanished = true;
        }

        currentTime += Time.deltaTime;

        if (vanished && currentTime > 7)
        {
            a += 0.06f;
            a = (a > 1) ? 1 : a;
            text.color = new Color(originalColor.r, originalColor.g, originalColor.b,
                a);
            if (a == 1)
            {
                vanished = false;
            }
        }
        if (!vanished && currentTime > 11 && !done)
        {
            a -= 0.06f;
            a = (a < 0) ? 0 : a;
            text.color = new Color(originalColor.r, originalColor.g, originalColor.b,
                a);
            if (a == 0)
            {
                done = true;
            }
        }
    }
}
