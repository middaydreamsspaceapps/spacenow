﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxController : MonoBehaviour
{
   public GameObject Camera;
    public Material[] materials;
   // public GameObject buttonParent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   public void OnPresButton(int index)
    {
        if (index>materials.Length-1)
        {
            return;
        }
        Camera.GetComponent<Skybox>().material = materials[index] ;
    }
}
