﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowDescription : MonoBehaviour
{

    public float delay;
    private string updateText;
    private string[] cometTexts = {
    "A comet is a small body in our solar system that orbits the Sun much as do the Earth and the planets",
    "A comet has a nucleus, which is a solid body, usually around 1-10 km across and is probably made of ices and dust and rock",
    "For this reason, some people refer to them as dirty snowballs",
    "As that ball of ice gets close enough to the Sun, its heat begins to melt some of the ice that makes up the comet",
    "The melted ice becomes a gaseous tail that extends away from the source of the heat. The tail is pushed out by the Sun's solar wind",
    "The most famous comet is Halley's comet, which appears to be visible from earth every 76 years",
    "The comet's periodicity was first determined in 1705 by English astronomer Edmond Halley, after whom it is now named"};

    void Start()
    {
        StartCoroutine(ShowCometText());
    }

    IEnumerator ShowCometText()
    {
        for (int j = 0; j < cometTexts.Length; j++)
        {
            yield return new WaitForSeconds(3);
            for (int i = 0; i < cometTexts[j].Length; i++)
            {
                updateText = cometTexts[j].Substring(0, i + 1);
                GetComponent<Text>().text = updateText;
                yield return new WaitForSeconds(delay);
            }
        }
        yield return new WaitForSeconds(3);
        gameObject.SetActive(false);

    }
}