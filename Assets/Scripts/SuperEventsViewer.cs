﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperEventsViewer : MonoBehaviour
{


    // List<SuperEvent> superEvents;

    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadJsonData();
        }
    }

    public void LoadJsonData()
    {
        var jsonAsset = Resources.Load<TextAsset>("SuperEvents");
        string json = jsonAsset.ToString();
     
        Debug.Log(jsonAsset);
    }
}


[System.Serializable]
public class SuperEvent
{
    public string superevent_id;
    public string category;
    public string created;
    public string t_start;
    public string t_end;
  
}
