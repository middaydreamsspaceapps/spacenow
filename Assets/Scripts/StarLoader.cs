﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

[System.Serializable]
public class Star
{
    public int id { get; set; }
    public string name { get; set; }
    public string serial_no { get; set; }
    public double ra_original { get; set; }
    public string right_ascension { get; set; }
    public double declination { get; set; }
    public double distance { get; set; }
    public double magnitude { get; set; }
    public string spectrum { get; set; }
    public string star_color { get; set; }
    public float star_x { get; set; }
    public float star_y { get; set; }
    public float star_z { get; set; }
    public double color_index { get; set; }
    public double x_co { get; set; }
    public double y_co { get; set; }
    public double z_co { get; set; }
    public int instance_id { get; set; }
}

public class StarLoader : MonoBehaviour
{

    public void UseGyro()
    {
        Camera.main.transform.Rotate(-Input.gyro.rotationRateUnbiased.x, -Input.gyro.rotationRateUnbiased.y, 0f);
    }
    public ArrayList getColor(string the_spectrum)
    {
        string the_color = "#9bb0ff";
        float[] color_values = { 155f, 176f, 255f };
        switch (the_spectrum.ToCharArray()[0])
        {
            case 'b':
                the_color = "#aabfff";
                color_values[0] = 170f;
                color_values[1] = 191f;
                color_values[2] = 255f;
                break;
            case 'c':
                the_color = "#cad7ff";
                color_values[0] = 202f;
                color_values[1] = 215f;
                color_values[2] = 255f;
                break;
            case 'f':
                the_color = "#f8f7ff";
                color_values[0] = 248f;
                color_values[1] = 247f;
                color_values[2] = 255f;
                break;
            case 'g':
                the_color = "#fff4ea";
                color_values[0] = 255f;
                color_values[1] = 244f;
                color_values[2] = 234f;
                break;
            case 'k':
                the_color = "#ffd2a1";
                color_values[0] = 255f;
                color_values[1] = 210f;
                color_values[2] = 161f;
                break;
            case 'm':
                the_color = "#ffcc6f";
                color_values[0] = 255f;
                color_values[1] = 204f;
                color_values[2] = 111f;
                break;
        }
        ArrayList colorValues = new ArrayList();
        colorValues.Add(the_color);
        colorValues.Add(color_values[0]);
        colorValues.Add(color_values[1]);
        colorValues.Add(color_values[2]);
        return colorValues;
    }

    string secondToHour(double totalSeconds)
    {
        int hours = (int)(totalSeconds / 3600);
        totalSeconds %= 3600;
        int minutes = (int)(totalSeconds / 60);
        int seconds = (int)totalSeconds % 60;
        string the_time = hours + "h";
        if (minutes != 0)
        {
            the_time += " " + minutes + "m";
        }
        if (seconds != 0)
        {
            the_time += " " + seconds + "s";
        }
        return the_time;
    }

    public GameObject star_prefab, maincanvas;
    public ArrayList stars = new ArrayList();
    public float sensitivity = 0.0001f;
    public float maxYAngle = 85f;
    private Vector2 currentRotation;
    public Text nameText, rightAscensionText, declinationText, distencetText, magnitudeText, spectrumText;
    public Slider zoomSlider;

    void Start()
    {
        Input.gyro.enabled = true;
        zoomSlider.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
        TextAsset textAsset = new TextAsset();
        textAsset = Resources.Load<TextAsset>("star_data");
        string[] all_stars = textAsset.text.Split('\n');

        int ik = 0;

        string starNames = "";

        while (ik < all_stars.Length)
        {
            var line = all_stars[ik];
            var star_details = line.Split(',');
            if (star_details.Length >= 7)
            {
                if (star_details[6].Length != 0)
                {
                    Star aStar = new Star();
                    aStar.id = int.Parse(star_details[0]);
                    aStar.serial_no = star_details[2];
                    aStar.name = star_details[6];
                    starNames += "HD" + aStar.serial_no + ": " + aStar.name + ",\n";
                    aStar.ra_original = double.Parse(star_details[7]);
                    aStar.right_ascension = secondToHour(double.Parse(star_details[7]) * 3600);
                    aStar.declination = double.Parse(star_details[8]);
                    aStar.distance = double.Parse(star_details[9]);
                    aStar.magnitude = double.Parse(star_details[13]);
                    aStar.spectrum = star_details[15];
                    aStar.star_color = getColor(star_details[15].ToLower())[0].ToString();
                    aStar.star_x = float.Parse(getColor(star_details[15].ToLower())[1].ToString());
                    aStar.star_y = float.Parse(getColor(star_details[15].ToLower())[2].ToString());
                    aStar.star_z = float.Parse(getColor(star_details[15].ToLower())[3].ToString());
                    aStar.color_index = double.Parse(star_details[16]);
                    aStar.x_co = double.Parse(star_details[17]);
                    aStar.y_co = double.Parse(star_details[18]);
                    aStar.z_co = double.Parse(star_details[19]);
                    stars.Add(aStar);
                    GameObject the_star = Instantiate(star_prefab, new Vector3((float)aStar.x_co * 8f, (float)aStar.y_co * 8f,
                    (float)aStar.z_co) * 8f, Quaternion.identity);
                    the_star.name = aStar.id + "," + aStar.name + "," + aStar.ra_original + "," + aStar.right_ascension + "," + aStar.declination
                            + "," + (aStar.distance * 3.26) + "," + aStar.magnitude + "," + aStar.spectrum + "," + aStar.star_color + "," + aStar.star_x
                            + "," + aStar.star_y + "," + aStar.star_z + "," + aStar.color_index + "," + aStar.x_co + "," + aStar.y_co
                            + "," + aStar.z_co;
                    for (int i = 0; i <= 2; i++)
                    {
                        GameObject star_child = the_star.transform.GetChild(i).gameObject;
                        star_child.GetComponent<SpriteRenderer>().color = new Color((aStar.star_x / 255),
                                (aStar.star_y / 255), (aStar.star_z / 255), 1f);
                    }
                }
            }
            ik++;
        }
    }

    void Update()
    {
        UseGyro();
        /*currentRotation.x += Input.GetAxis("Mouse X") * 1.5f;
        currentRotation.y -= Input.GetAxis("Mouse Y") * 1.5f;
        currentRotation.x = Mathf.Repeat(currentRotation.x, 360);
        currentRotation.y = Mathf.Clamp(currentRotation.y, -maxYAngle, maxYAngle);
        Camera.main.transform.rotation = Quaternion.Euler(currentRotation.y, currentRotation.x, 0);*/

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 10000f))
            {
                string[] star_properties = hit.transform.name.Split(',');
                nameText.text = star_properties[1];
                rightAscensionText.text = star_properties[3];
                declinationText.text = star_properties[4];
                distencetText.text = star_properties[5] + " Light Years";
                magnitudeText.text = star_properties[6];
                spectrumText.text = star_properties[7];
                maincanvas.SetActive(true);
            }
        }
    }
    public void ValueChangeCheck()
    {
        Camera.main.fieldOfView = 60f - zoomSlider.value * 60f;
    }
}
