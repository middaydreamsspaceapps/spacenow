﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlackHoleDescriptor : MonoBehaviour
{
    public float delay, stoptime;
    private float time, scale, whiteScale, blastTime, brokenScale, outerAlpha, brokenTime, gasScale;
    private bool forceVanished, compared, blastStarted, rayShown, brokenShown, brokenStopped;

    public GameObject[] fermiGravity;
    public GameObject description, comparisonText, blastRay, mainStar,
                blackhole, whiteParticle, cinemachineHolder, mainPanel,
                brokenStar, cinemachineHolder2, brokenGas;

    private ParticleSystem gasParticle;

    Text descriptionText;
    private string[] blackHoleTexts = {
        "In a star, gravitational force pushes it's gas to the center"+
        " while fermi pressure and some other forces push it outside",
        "As long as both the forces are equal the star stays in a stable state",
        "The Hydrogen gas inside the star fuses and creates Helium which further fuses and finally creates an Iron core.",
        "At this stage the gravitational force becomes greater than the fermi pressure and it squeezes the star to it's center",
        "When squeezed, at the end of it's life, a star with a mass 3 times greater than the sun sometimes creates a big blast called supernova"+
        " which sometimes creates a blackhole",
        "A black hole is a region of spacetime exhibiting strong gravitational effects.",
        "Nothing, not even particles and electromagnetic radiation such as light can escape from it.",
        "The theory of general relativity predicts that a sufficiently compact mass can deform spacetime to form a black hole.",
        "At the center of a black hole, lies a gravitational singularity, a region where the spacetime curvature becomes infinite.",
        "An event horizon is a region in spacetime beyond which light cannot totally escape, because the gravitational pull of"+
        " a massive object becomes so great as to make escape impossible",
        "Hawking radiation is a radiation theoretically emitted from just outside the event horizon of a black hole. Stephen W. Hawking proposed"+
        " in 1974 that subatomic particle pairs arising naturally near the event horizon may"+
        " result in one particle’s escaping the vicinity of the black hole while the other particle, of negative energy, disappears into it",
        "The Schwarzschild radius is the radius of the event horizon surrounding a non-rotating black hole."+
            " Any object with a physical radius smaller than its Schwarzschild radius will be a black hole."+
            " This quantity was first derived by Karl Schwarzschild in 1916",
        " ",
        " "
    };


    void Start()
    {
        descriptionText = description.GetComponent<Text>();
        scale = mainStar.transform.localScale.x;
        whiteScale = whiteParticle.transform.localScale.x;
        brokenScale = brokenStar.transform.localScale.x;
        gasParticle = brokenGas.GetComponent<ParticleSystem>();
        StartCoroutine(SholwBlackholeDescription());
    }

    void Update()
    {
        time += Time.deltaTime;

        if (blastStarted)
        {
            blastTime += Time.deltaTime;
            //Debug.Log(blastTime);
        }

        if (compared && scale > 0.8f)
        {
            scale = mainStar.transform.localScale.x;
            scale -= 0.001f;
            mainStar.transform.localScale = new Vector3(scale, scale, scale);
        }
        if (compared && whiteScale > 0.6f)
        {
            whiteScale = whiteParticle.transform.localScale.x;
            whiteScale -= 0.001f;
            whiteParticle.transform.localScale = new Vector3(whiteScale, whiteScale, whiteScale);
        }

        if(brokenShown){
            brokenTime += Time.deltaTime;
        }

        if (blastTime > 6)
        {
            blackhole.SetActive(true);
            rayShown = true;
        }

        if (blastTime > 8)
        {
            cinemachineHolder.SetActive(true);
        }

        if (rayShown)
        {
            outerAlpha += (outerAlpha >= 1) ? 0 : 0.004f;
            outerAlpha = (outerAlpha > 1) ? 1 : outerAlpha;
        }

        if (brokenShown && brokenScale > 0.0f && brokenTime>6f)
        {
            brokenScale = brokenStar.transform.localScale.x;
            brokenScale -= 0.03f;
            brokenStar.transform.localScale = new Vector3(brokenScale, brokenScale, brokenScale);
        }else if (brokenScale<=0.0f){
            gasParticle.Stop();
            if (!brokenStopped){
                StartCoroutine(RemoveInhaleText());
                brokenStopped = true;
            }
        }
    }

    IEnumerator RemoveInhaleText()
    {
        yield return new WaitForSeconds(4f);
        brokenStar.SetActive(false);
        yield return null;
    }

    IEnumerator SholwBlackholeDescription()
    {
        for (int i = 0; i < blackHoleTexts.Length; i++)
        {

            for (int j = 0; j < blackHoleTexts[i].Length; j++)
            {
                descriptionText.text = blackHoleTexts[i].Substring(0, j + 1);
                if (i == 0 && j == 29)
                {
                    fermiGravity[2].SetActive(true);
                    fermiGravity[3].SetActive(true);
                }
                if (i == 0 && j == 68)
                {
                    fermiGravity[0].SetActive(true);
                    fermiGravity[1].SetActive(true);
                }
                if (i == 2 && !forceVanished)
                {
                    fermiGravity[0].SetActive(false);
                    fermiGravity[1].SetActive(false);
                    fermiGravity[2].SetActive(false);
                    fermiGravity[3].SetActive(false);
                }
                if (i == 3)
                {
                    comparisonText.SetActive(true);
                }
                if (i == 3 && j == 60 && !compared)
                {
                    compared = true;
                }
                if (i == 4 && j == 20)
                {
                    comparisonText.SetActive(false);
                }
                if (i == 4 && j == 66)
                {
                    blastRay.SetActive(true);
                    mainStar.SetActive(false);
                    blastStarted = true;

                }
                if (i == 8)
                {
                    fermiGravity[4].SetActive(true);
                    fermiGravity[5].SetActive(true);
                }
                if (i == 9 && j == 0)
                {
                    fermiGravity[4].SetActive(false);
                    fermiGravity[5].SetActive(false);
                    fermiGravity[8].SetActive(true);
                    fermiGravity[9].SetActive(true);
                }
                if (i == 10)
                {

                    fermiGravity[8].SetActive(false);
                    fermiGravity[9].SetActive(false);
                    fermiGravity[6].SetActive(true);
                    fermiGravity[7].SetActive(true);
                }
                if (i == 11)
                {
                    fermiGravity[6].SetActive(false);
                    fermiGravity[7].SetActive(false);
                    fermiGravity[10].SetActive(true);
                    fermiGravity[11].SetActive(true);
                    fermiGravity[12].SetActive(true);
                    fermiGravity[13].SetActive(true);
                    fermiGravity[14].SetActive(true);
                }
                if (i == 12)
                {
                    fermiGravity[10].SetActive(false);
                    fermiGravity[11].SetActive(false);
                    fermiGravity[12].SetActive(false);
                    fermiGravity[13].SetActive(false);
                    fermiGravity[14].SetActive(false);
                    cinemachineHolder2.SetActive(true);
                    brokenStar.SetActive(true);
                    brokenShown = true;
                }
                if(i==13){
                    mainPanel.SetActive(false);
                }
                yield return new WaitForSeconds(delay);
            }
            yield return new WaitForSeconds(stoptime);
        }
        yield return null;
    }


}
