﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowBHProperties : MonoBehaviour {


    public GameObject singularity, eventHorizon, hawkingRadiation;
    private float currentTime;
    private bool singAppeared, eHorizonAppeared, hRadiationAppeared;

	void Start () {
		
	}
	
	
	void Update () {
        currentTime += Time.deltaTime;

        if (currentTime >= 4 && !singAppeared)
        {
            singularity.SetActive(true);
            singAppeared = true;
        }
        if (currentTime >= 7 && !eHorizonAppeared)
        {
            singularity.SetActive(false);
            eventHorizon.SetActive(true);
            eHorizonAppeared = true;
        }
        if(currentTime>=10 && !hRadiationAppeared)
        {
            eventHorizon.SetActive(false);
            hawkingRadiation.SetActive(true);
            hRadiationAppeared = true;
        }
        if(currentTime >= 13)
        {
            hawkingRadiation.SetActive(false);
        }
	}
}
